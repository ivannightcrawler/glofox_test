import React, {useEffect} from 'react';
import {
  StyleSheet,
  useColorScheme,
  View,
  ActivityIndicator,
} from 'react-native';
import {useAppDispatch} from './hooks/redux';
import Colors from './assets/colors';
import {enableBottomTabs} from './screenNavigation';
import {loadBeerList, loadRandomBeer} from './redux/beersSlice';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.dark.primary : Colors.light.primary,
  };

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(loadRandomBeer({isAlcoholic: false})).then(() => {
      enableBottomTabs(isDarkMode ? Colors.dark.primary : Colors.light.primary);
    });

    dispatch(loadBeerList({pageNumber: 1}));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={[styles.container, backgroundStyle]}>
      <ActivityIndicator size="large" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default App;
