import {by, device, element, expect, waitFor} from 'detox';

describe('App Tests', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  describe('Random Beer Screen', () => {
    it('should display the random beer screen', async () => {
      const scrollView = element(by.id('RandomBeer.View'));
      await expect(scrollView).toExist();
      await expect(scrollView).toBeVisible();
    });

    it('navigation items should be present', async () => {
      await expect(element(by.id('Menu.BeerListScreen'))).toExist();
      await expect(element(by.id('Menu.RandomBeerScreen'))).toExist();
    });

    it('should be able to navigate to beer list screen', async () => {
      await element(by.id('Menu.BeerListScreen')).tap();
      await waitFor(element(by.id('BeerList.View')))
        .toBeVisible()
        .withTimeout(2000);
    });
  });

  describe('Beer List Screen', () => {
    it('date filter should be present', async () => {
      const datePicker = element(by.id('BeerList.DatePicker'));
      await expect(datePicker).toExist();
      await expect(datePicker).toBeVisible();
    });
  });
});
