import {base_url} from './setup';
import Beer from '../types/Beer';

export const getRandomBeer = async (
  isAlcoholic = false,
): Promise<Beer | null> => {
  let url = `${base_url}/beers/random`;

  if (!isAlcoholic) {
    url += '?abv_lt=1';
  }

  return fetch(url)
    .then(res => res.json())
    .then(beers => beers[0])
    .catch(e => {
      console.error('Error while calling getRandomBeer()', e);
      return Promise.reject(null);
    });
};

export const getBeerList = async (): Promise<Beer[]> => {
  const url = `${base_url}/beers`;

  return fetch(url)
    .then(res => res.json())
    .catch(e => {
      console.error('Error while calling getBeerList()', e);
      return Promise.reject([]);
    });
};
