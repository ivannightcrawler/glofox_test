const Colors = {
  light: {
    primary: '#3DB2FF',
    secondary: '#FFEDDA',
    light: '#FFEDDA',
    dark: '#FF2442',
  },
  dark: {
    primary: '#FFE194',
    secondary: '#E8F6EF',
    light: '#B8DFD8',
    dark: '#4C4C6D',
  },
};

export default Colors;
