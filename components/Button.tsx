import React from 'react';
import {Pressable, StyleSheet, Text, useColorScheme} from 'react-native';
import Colors from '../assets/colors';

interface Props {
  title: string;
  onPress: () => void;
}
const AppButton: React.FC<Props> = ({title, onPress}) => {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <Pressable
      onPress={onPress}
      style={[
        isDarkMode ? styles.darkThemeWrapper : styles.lightThemeWrapper,
        styles.wrapper,
      ]}>
      <Text
        style={[
          isDarkMode ? styles.darkThemeText : styles.lightThemeText,
          styles.textStyle,
        ]}>
        {title}
      </Text>
    </Pressable>
  );
};

export default AppButton;

const styles = StyleSheet.create({
  wrapper: {
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    width: '80%',
  },
  darkThemeText: {
    color: Colors.dark.dark,
  },
  lightThemeText: {
    color: Colors.light.dark,
  },
  textStyle: {
    textAlign: 'center',
  },
  darkThemeWrapper: {
    backgroundColor: Colors.dark.secondary,
    borderColor: Colors.dark.dark,
  },
  lightThemeWrapper: {
    backgroundColor: Colors.light.secondary,
    borderColor: Colors.dark.dark,
  },
});
