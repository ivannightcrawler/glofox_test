import React from 'react';
import {SafeAreaView, StyleSheet, useColorScheme, View} from 'react-native';
import Colors from '../../assets/colors';

const ScreenLayout: React.FC = ({children}) => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.dark.primary : Colors.light.primary,
  };

  return (
    <SafeAreaView style={[styles.sectionContainer, backgroundStyle]}>
      <View style={styles.wrapper}>{children}</View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  wrapper: {
    padding: 10,
  },
});

export default ScreenLayout;
