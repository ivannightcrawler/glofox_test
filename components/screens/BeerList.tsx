/**
 * Display a Beer List
 */

import React, {useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {useAppSelector} from '../../hooks/redux';
import Beer from '../../types/Beer';
import ScreenLayout from '../layout/ScreenLayout';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import DatePicker from 'react-native-date-picker';

const BeerItem = ({beer}: {beer: Beer}) => {
  // name of the beer, image and an abbreviated description of the bee
  return (
    <View style={styles.beerItemWrapper}>
      <View style={styles.beerItemLeft}>
        {beer.image_url ? (
          <FastImage
            style={styles.beerImage}
            source={{
              uri: beer?.image_url,
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
        ) : (
          <Text>NO IMG</Text>
        )}
      </View>
      <View style={styles.beerItemRight}>
        <View>
          <Text>Name: {beer.name}</Text>
        </View>
        <View>
          <Text>{beer.tagline}</Text>
        </View>
        <View>
          <Text>{beer.first_brewed}</Text>
        </View>
      </View>
    </View>
  );
};

const BeerList = () => {
  const [filterDate, setFilterDate] = useState(new Date());

  const beerList = useAppSelector(store => {
    return store.beers.beerList.filter(beer => {
      return moment(beer.first_brewed, 'MM/YYYY').isBefore(moment(filterDate));
    });
  });

  const renderItem = ({item}: {item: Beer}) => <BeerItem beer={item} />;

  return (
    <ScreenLayout>
      <View style={styles.wrapper} testID="BeerList.View">
        <View style={styles.dateWraper}>
          <Text style={styles.dateTitle}>Show only Beers brewed before</Text>
          <DatePicker
            testID="BeerList.DatePicker"
            date={filterDate}
            mode={'date'}
            fadeToColor="none"
            maximumDate={new Date()}
            onDateChange={setFilterDate}
          />
        </View>
        <FlatList
          data={beerList}
          renderItem={renderItem}
          keyExtractor={item => String(item.id)}
        />
      </View>
    </ScreenLayout>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    width: '100%',
  },
  wrapper: {
    justifyContent: 'flex-start',
    height: '100%',
  },
  dateWraper: {
    paddingTop: 10,
    alignItems: 'center',
  },
  dateTitle: {
    fontSize: 24,
  },
  beerItemWrapper: {
    paddingVertical: 10,
    flexDirection: 'row',
  },
  beerItemLeft: {
    width: '20%',
    height: 50,
  },
  beerItemRight: {
    width: '80%',
  },
  beerImage: {
    width: '100%',
    height: '100%',
  },
});

export default BeerList;
