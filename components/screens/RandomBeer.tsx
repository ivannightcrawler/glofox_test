/**
 * Display a Random Beer
 */

import React from 'react';
import {ScrollView, StyleSheet, Text, useColorScheme, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import Colors from '../../assets/colors';
import {useAppSelector, useAppDispatch} from '../../hooks/redux';
import Button from '../Button';
import {loadRandomBeer} from '../../redux/beersSlice';
import ScreenLayout from '../layout/ScreenLayout';

const RandomBeer = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const dividerStyle = {
    backgroundColor: isDarkMode
      ? Colors.dark.secondary
      : Colors.light.secondary,
  };

  const dispatch = useAppDispatch();

  const scroll = React.useRef<ScrollView>(null);

  const scrollToTop = () => {
    scroll.current?.scrollTo({x: 0, y: 0, animated: true});
  };

  const displayRandomBeer = () => {
    scrollToTop();
    dispatch(loadRandomBeer({isAlcoholic: true}));
  };

  const displayNonAlcholicBeer = () => {
    scrollToTop();
    dispatch(loadRandomBeer({isAlcoholic: false}));
  };

  const activeBeer = useAppSelector(store => {
    return store.beers.randomBeer;
  });

  return (
    <ScreenLayout>
      <ScrollView
        ref={scroll}
        testID="RandomBeer.View"
        alwaysBounceVertical={false}
        contentInsetAdjustmentBehavior="automatic">
        <Text style={styles.pageTitle}>{activeBeer?.name}</Text>
        <Text style={styles.pageSubtitle}>{activeBeer?.tagline}</Text>
        <View style={styles.imageWrapper}>
          {activeBeer?.image_url ? (
            <FastImage
              style={styles.image}
              source={{
                uri: activeBeer?.image_url,
              }}
              resizeMode={FastImage.resizeMode.contain}
            />
          ) : (
            <Text>No Image Availabe</Text>
          )}
        </View>
        <Text style={styles.pageDescription}>{activeBeer?.description}</Text>
        <View style={[styles.divider, dividerStyle]} />
        <Text style={styles.pageSubtitle}>
          Want to discover another beer?🍺
        </Text>
        <View style={styles.buttonsWrapper}>
          <Button title="Yes Please!🍻" onPress={displayRandomBeer} />
          <View style={styles.buttonSeparator} />
          <Button
            title="Yes! Non Alcoholic Though!🍻"
            onPress={displayNonAlcholicBeer}
          />
        </View>
      </ScrollView>
    </ScreenLayout>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 24,
    textAlign: 'center',
  },
  pageSubtitle: {
    fontSize: 18,
    textAlign: 'center',
    padding: 10,
  },
  pageDescription: {
    fontSize: 16,
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
  },
  imageWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 400,
  },
  divider: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  buttonsWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonSeparator: {
    height: 10,
  },
});

export default RandomBeer;
