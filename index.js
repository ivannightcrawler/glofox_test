/**
 * @format
 */
import React from 'react';
import App from './App';
import {Navigation} from 'react-native-navigation';
import {loadingAppScreen, randomBeerScreen, beerListScreen} from './screens';
import BeerList from './components/screens/BeerList';
import RandomBeer from './components/screens/RandomBeer';
import {Provider} from 'react-redux';
import store from './redux/store';

Navigation.registerComponent(
  loadingAppScreen.name,
  () => props =>
    (
      <Provider store={store}>
        <App {...props} />
      </Provider>
    ),
  () => App,
);

Navigation.registerComponent(
  randomBeerScreen.name,
  () => props =>
    (
      <Provider store={store}>
        <RandomBeer {...props} />
      </Provider>
    ),
  () => RandomBeer,
);

Navigation.registerComponent(
  beerListScreen.name,
  () => props =>
    (
      <Provider store={store}>
        <BeerList {...props} />
      </Provider>
    ),
  () => BeerList,
);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: loadingAppScreen.name,
              options: {
                topBar: {
                  visible: false,
                },
              },
            },
          },
        ],
      },
    },
  });
});
