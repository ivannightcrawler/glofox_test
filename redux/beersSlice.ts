import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {getRandomBeer, getBeerList} from '../api/main';
import Beer from '../types/Beer';

// Define a type for the slice state
interface BeersState {
  randomBeer: Beer | null;
  beerList: Beer[];
}

// Define the initial
const initialState: BeersState = {
  randomBeer: null,
  beerList: [],
};

// Thunk to load a random beer
export const loadRandomBeer = createAsyncThunk<
  Beer | null,
  {isAlcoholic: boolean},
  {
    rejectValue: null;
  }
>('beers/loadRandomBeer', async ({isAlcoholic}) => {
  return await getRandomBeer(isAlcoholic);
});

// Thunk to load the beer list
export const loadBeerList = createAsyncThunk<
  Beer[],
  {},
  {
    rejectValue: null;
  }
>('beers/loadBeerList', async () => {
  return await getBeerList();
});

export const beerSlice = createSlice({
  name: 'beers',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(loadRandomBeer.fulfilled, (state, action) => {
        state.randomBeer = action.payload;
      })
      .addCase(loadBeerList.fulfilled, (state, action) => {
        state.beerList = action.payload;
      });
  },
});

export default beerSlice.reducer;
