import {configureStore} from '@reduxjs/toolkit';

import beerReducer from './beersSlice';

const store = configureStore({
  reducer: {
    beers: beerReducer,
  },
});

export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;

// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
