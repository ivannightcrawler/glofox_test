import {Navigation} from 'react-native-navigation';
import {beerListScreen, randomBeerScreen} from './screens';

export const enableBottomTabs = (bgColor: string) => {
  Navigation.setRoot({
    root: {
      bottomTabs: {
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: randomBeerScreen.name,
                    options: {
                      bottomTabs: {
                        backgroundColor: bgColor,
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTab: {
                  testID: 'Menu.RandomBeerScreen',
                  icon: require('./assets/beer_full.png'),
                  iconColor: '#000',
                  selectedIconColor: '#FFF',
                  iconInsets: {
                    top: 10,
                    bottom: 0,
                    left: 0,
                    right: 0,
                  },
                },
                topBar: {
                  visible: false,
                },
              },
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: beerListScreen.name,
                    options: {
                      bottomTabs: {
                        backgroundColor: bgColor,
                      },
                    },
                  },
                },
              ],
              options: {
                bottomTab: {
                  testID: 'Menu.BeerListScreen',
                  icon: require('./assets/beer_list.png'),
                  iconColor: '#000',
                  selectedIconColor: '#FFF',
                  iconInsets: {
                    top: 10,
                    bottom: 0,
                    left: 0,
                    right: 0,
                  },
                },
                topBar: {
                  visible: false,
                },
              },
            },
          },
        ],
      },
    },
  });
};
