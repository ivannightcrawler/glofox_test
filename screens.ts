export type AppScreen = {
  name: string;
  title?: string;
};

// MAIN View
export const loadingAppScreen: AppScreen = {
  name: 'com.glofox.LoadingApp',
};
export const randomBeerScreen: AppScreen = {
  name: 'com.glofox.RandomBeer',
  title: 'A Beer a day keeps the doctor away',
};
export const beerListScreen: AppScreen = {
  name: 'com.glofox.BeerList',
  title: '',
};
