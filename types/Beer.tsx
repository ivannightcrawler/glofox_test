export default interface Beer {
  id: number;
  name: string;
  tagline: string;
  description: string;
  first_brewed: string;
  image_url: string;
};
